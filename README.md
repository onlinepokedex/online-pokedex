# online-pokedex

[![pipeline status](https://gitlab.com/onlinepokedex/online-pokedex/badges/master/pipeline.svg)](https://gitlab.com/onlinepokedex/online-pokedex/-/commits/master)
[![coverage report](https://gitlab.com/onlinepokedex/online-pokedex/badges/master/coverage.svg)](https://gitlab.com/onlinepokedex/online-pokedex/-/commits/master)

A site where you can overview any informations on any pokémon in a go and create your dream team with ease and balance
