package com.pokedex.project.auth.controller;

import com.pokedex.project.auth.form.LoginForm;
import com.pokedex.project.auth.model.User;
import com.pokedex.project.auth.service.AuthService;
import com.pokedex.project.exception.AuthenticationFailedException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.exception.UserIdAlreadyExistException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping(path = "/pokedex/api/v1/auth/")
/**
 * AuthController
 */
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping(path = "/register")
    public ResponseEntity<User> registerUser(@Valid @RequestBody User user)
            throws UserIdAlreadyExistException {
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(authService.registerUser(user));
    }

    @PostMapping(path = "/authenticate")
    public ResponseEntity<User> authenticateUser(@Valid @RequestBody LoginForm loginForm)
            throws AuthenticationFailedException, UserDidNotExistException {
        User user = authService.authenticateUser(
                loginForm.getUserId(),
                loginForm.getUserPassword()
        );
        return ResponseEntity.ok().body(user);
    }
}
