package com.pokedex.project.auth.form;

public class LoginForm {

    private String userId;
    private String userPassword;

    public LoginForm() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
