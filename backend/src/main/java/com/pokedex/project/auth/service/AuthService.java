package com.pokedex.project.auth.service;

import com.pokedex.project.auth.model.User;
import com.pokedex.project.exception.AuthenticationFailedException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.exception.UserIdAlreadyExistException;

import java.util.List;

public interface AuthService {

    User registerUser(User user) throws  UserIdAlreadyExistException;

    User findUserById(String userId) throws UserDidNotExistException;

    User authenticateUser(String userId, String userPassword)
            throws UserDidNotExistException, AuthenticationFailedException;
}
