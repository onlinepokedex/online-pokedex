package com.pokedex.project.auth.service;

import com.pokedex.project.auth.model.User;
import com.pokedex.project.auth.repository.UserRepository;
import com.pokedex.project.exception.AuthenticationFailedException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.exception.UserIdAlreadyExistException;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    public AuthServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User registerUser(User user) throws UserIdAlreadyExistException {
        try {
            findUserById(user.getUserId());
            throw new UserIdAlreadyExistException("User with id '"
                    + user.getUserId() + "' already exist");
        } catch (UserDidNotExistException e) {
            return userRepository.save(user);
        }
    }

    @Override
    public User findUserById(String userId) throws UserDidNotExistException {
        User user = userRepository.findByUserId(userId).orElseThrow(
                () -> new UserDidNotExistException("User with id '" + userId + "' did not exist")
        );
        return user;
    }

    @Override
    public User authenticateUser(String userId, String userPassword)
            throws UserDidNotExistException, AuthenticationFailedException {
        User user = findUserById(userId);
        if (userPassword.equals(user.getUserPassword())) {
            return user;
        }
        throw new AuthenticationFailedException("Password doesn't match with given user id");
    }
}
