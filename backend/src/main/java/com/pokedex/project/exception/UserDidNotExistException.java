package com.pokedex.project.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UserDidNotExistException extends Exception {

    public UserDidNotExistException(String message) {
        super(message);
    }
}
