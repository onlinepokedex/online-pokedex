package com.pokedex.project.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserIdAlreadyExistException extends Exception {

    public UserIdAlreadyExistException(String message) {
        super(message);
    }
}
