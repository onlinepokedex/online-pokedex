package com.pokedex.project.pokemonlist.controller;

import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.pokemonlist.model.Pokemon;
import com.pokedex.project.pokemonlist.service.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping(path = "/pokedex/api/v1/pokemon/")
public class PokemonController {

    @Autowired
    private PokemonService pokemonService;

    @GetMapping("/")
    public List<Pokemon> getAllPokemon() {
        return pokemonService.findAllPokemon();
    }

    @PostMapping("/")
    public ResponseEntity<Pokemon> createPokemon(@Valid @RequestBody Pokemon pokemon) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(pokemonService.createPokemon(pokemon));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Pokemon> getPokemonById(@PathVariable(value = "id") Integer pokemonId)
            throws ResourceNotFoundException {
        Pokemon pokemon = pokemonService.findPokemonById(pokemonId);
        return ResponseEntity.ok().body(pokemon);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePokemonById(@PathVariable(value = "id") Integer pokemonId)
            throws ResourceNotFoundException {
        pokemonService.deletePokemon(pokemonId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


}
