package com.pokedex.project.pokemonlist.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

@Entity
@Table(name = "pokemon")
public class Pokemon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;
    @JsonProperty("primary_type")
    @Column(name = "primary_type")
    private String primaryType;
    @JsonProperty("secondary_type")
    @Column(name = "secondary_type")
    private String secondaryType;
    @Column(name = "height")
    private Double height;
    @Column(name = "weight")
    private Double weight;
    @Column(name = "sprite")
    private String sprite;
    @Column(name = "hp")
    private Integer hp;
    @Column(name = "attack")
    private Integer attack;
    @Column(name = "defense")
    private Integer defense;
    @JsonProperty("special_attack")
    @Column(name = "special_attack")
    private Integer specialAttack;
    @JsonProperty("special_defense")
    @Column(name = "special_defense")
    private Integer specialDefense;
    @Column(name = "speed")
    private Integer speed;
    @Column(name = "species")
    private String species;
    @ElementCollection
    @Column(name = "abilities")
    private List<String> abilities;
    @ElementCollection
    @CollectionTable(name = "type_weakness", joinColumns = { @JoinColumn(name = "pokemon_id") })
    @MapKeyColumn(name = "type")
    @Column(name = "weakness_multiplier")
    @JsonProperty("weakness_multiplier")
    private Map<String, Double> weaknessMultiplier = new HashMap<>();

    public Pokemon() {

    }

    public Pokemon(String name, String primaryType, String secondaryType,
                   Double height, Double weight, String sprite,
                   Integer hp, Integer attack, Integer defense,
                   Integer specialAttack, Integer specialDefense, Integer speed,
                   String species, List<String> abilities, HashMap<String,
                   Double> weaknessMultiplier) {
        this.name = name;
        this.primaryType = primaryType;
        this.secondaryType = secondaryType;
        this.height = height;
        this.weight = weight;
        this.sprite = sprite;
        this.hp = hp;
        this.attack = attack;
        this.defense = defense;
        this.specialAttack = specialAttack;
        this.specialDefense = specialDefense;
        this.speed = speed;
        this.species = species;
        this.abilities = abilities;
        this.weaknessMultiplier = weaknessMultiplier;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrimaryType() {
        return primaryType;
    }

    public void setPrimaryType(String primaryType) {
        this.primaryType = primaryType;
    }

    public String getSecondaryType() {
        return secondaryType;
    }

    public void setSecondaryType(String secondaryType) {
        this.secondaryType = secondaryType;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getSprite() {
        return sprite;
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public Integer getHp() {
        return hp;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public Integer getDefense() {
        return defense;
    }

    public void setDefense(Integer defense) {
        this.defense = defense;
    }

    public Integer getSpecialAttack() {
        return specialAttack;
    }

    public void setSpecialAttack(Integer specialAttack) {
        this.specialAttack = specialAttack;
    }

    public Integer getSpecialDefense() {
        return specialDefense;
    }

    public void setSpecialDefense(Integer specialDefense) {
        this.specialDefense = specialDefense;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public List<String> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<String> abilities) {
        this.abilities = abilities;
    }

    public Map<String, Double> getWeaknessMultiplier() {
        return weaknessMultiplier;
    }

    public void setWeaknessMultiplier(Map<String, Double> weaknessMultiplier) {
        this.weaknessMultiplier = weaknessMultiplier;
    }

}