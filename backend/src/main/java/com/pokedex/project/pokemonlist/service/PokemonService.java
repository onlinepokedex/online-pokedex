package com.pokedex.project.pokemonlist.service;

import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.pokemonlist.model.Pokemon;

import java.util.List;

public interface PokemonService {
    List<Pokemon> findAllPokemon();

    Pokemon findPokemonById(Integer id) throws ResourceNotFoundException;

    Pokemon createPokemon(Pokemon pokemon);

    void deletePokemon(Integer id) throws ResourceNotFoundException;
}
