package com.pokedex.project.pokemonlist.service;

import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.pokemonlist.model.Pokemon;
import com.pokedex.project.pokemonlist.repository.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PokemonServiceImpl implements PokemonService {

    @Autowired
    private PokemonRepository pokemonRepository;

    @Override
    public List<Pokemon> findAllPokemon() {
        return pokemonRepository.findAll();
    }

    @Override
    public Pokemon findPokemonById(Integer id) throws ResourceNotFoundException {
        return pokemonRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Pokemon not found with id " + id)
        );
    }

    @Override
    public Pokemon createPokemon(Pokemon pokemon) {
        return pokemonRepository.save(pokemon);
    }

    @Override
    public void deletePokemon(Integer id) throws ResourceNotFoundException {
        Pokemon pokemon = pokemonRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Pokemon not found with id " + id)
        );

        pokemonRepository.delete(pokemon);
    }
}
