package com.pokedex.project.teambuilder.controller;

import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.teambuilder.model.Team;
import com.pokedex.project.teambuilder.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/pokedex/api/v1/team/")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping("/{user}")
    public List<Team> getAllTeamByUserId(@PathVariable(value = "user") String userId) {
        return teamService.findAllByUserId(userId);
    }

    @PostMapping("/{user}")
    public ResponseEntity<Team> createTeam(@PathVariable(value = "user") String userId, @Valid @RequestBody Team team)
        throws UserDidNotExistException {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(teamService.createTeam(team, userId));
    }

    @GetMapping("/{user}/{id}")
    public ResponseEntity<Team> getTeamByIdAndUserId(@PathVariable(value = "user") String userId, @PathVariable(value = "id") Integer id)
        throws ResourceNotFoundException {
        Team team = teamService.findTeamByIdAndUserId(id, userId);
        return ResponseEntity.ok().body(team);
    }

    @DeleteMapping("/{user}/{id}")
    public ResponseEntity<Void> deleteTeam(@PathVariable(value = "user") String userId, @PathVariable(value = "id") Integer id)
            throws ResourceNotFoundException {

        teamService.deleteTeam(id, userId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
