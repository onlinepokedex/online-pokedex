package com.pokedex.project.teambuilder.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pokedex.project.auth.model.User;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "team")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;
    @JsonProperty("first_pokemon")
    @Column(name = "first_pokemon")
    private Integer firstPokemon;
    @JsonProperty("second_pokemon")
    @Column(name = "second_pokemon")
    private Integer secondPokemon;
    @JsonProperty("third_pokemon")
    @Column(name = "third_pokemon")
    private Integer thirdPokemon;
    @JsonProperty("fourth_pokemon")
    @Column(name = "fourth_pokemon")
    private Integer fourthPokemon;
    @JsonProperty("fifth_pokemon")
    @Column(name = "fifth_pokemon")
    private Integer fifthPokemon;
    @JsonProperty("sixth_pokemon")
    @Column(name = "sixth_pokemon")
    private Integer sixthPokemon;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false, referencedColumnName = "userId")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private User user;

    public Team() {

    }

    public Team(String name, Integer firstPokemon, Integer secondPokemon, Integer thirdPokemon, Integer fourthPokemon, Integer fifthPokemon, Integer sixthPokemon) {
        this.name = name;
        this.firstPokemon = firstPokemon;
        this.secondPokemon = secondPokemon;
        this.thirdPokemon = thirdPokemon;
        this.fourthPokemon = fourthPokemon;
        this.fifthPokemon = fifthPokemon;
        this.sixthPokemon = sixthPokemon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFirstPokemon() {
        return firstPokemon;
    }

    public void setFirstPokemon(Integer firstPokemon) {
        this.firstPokemon = firstPokemon;
    }

    public Integer getSecondPokemon() {
        return secondPokemon;
    }

    public void setSecondPokemon(Integer secondPokemon) {
        this.secondPokemon = secondPokemon;
    }

    public Integer getThirdPokemon() {
        return thirdPokemon;
    }

    public void setThirdPokemon(Integer thirdPokemon) {
        this.thirdPokemon = thirdPokemon;
    }

    public Integer getFourthPokemon() {
        return fourthPokemon;
    }

    public void setFourthPokemon(Integer fourthPokemon) {
        this.fourthPokemon = fourthPokemon;
    }

    public Integer getFifthPokemon() {
        return fifthPokemon;
    }

    public void setFifthPokemon(Integer fifthPokemon) {
        this.fifthPokemon = fifthPokemon;
    }

    public Integer getSixthPokemon() {
        return sixthPokemon;
    }

    public void setSixthPokemon(Integer sixthPokemon) {
        this.sixthPokemon = sixthPokemon;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
