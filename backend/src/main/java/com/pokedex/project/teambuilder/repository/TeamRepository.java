package com.pokedex.project.teambuilder.repository;

import com.pokedex.project.teambuilder.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TeamRepository extends JpaRepository<Team, Integer> {

    List<Team> findByUserUserId(String userId);
    Optional<Team> findByIdAndUserUserId(Integer id, String userId);
}
