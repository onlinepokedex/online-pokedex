package com.pokedex.project.teambuilder.service;

import com.pokedex.project.auth.model.User;
import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.teambuilder.model.Team;

import java.util.List;

public interface TeamService {
    List<Team> findAllByUserId(String userId);

    Team createTeam(Team team, String userId) throws UserDidNotExistException;

    Team findTeamByIdAndUserId(Integer id, String userId) throws ResourceNotFoundException;

    void deleteTeam(Integer id, String userId) throws ResourceNotFoundException;

}
