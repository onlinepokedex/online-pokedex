package com.pokedex.project.teambuilder.service;

import com.pokedex.project.auth.repository.UserRepository;
import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.teambuilder.model.Team;
import com.pokedex.project.teambuilder.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Team> findAllByUserId(String userId) {
        return teamRepository.findByUserUserId(userId);
    }

    @Override
    public Team createTeam(Team team, String userId) throws UserDidNotExistException {
        return userRepository.findByUserId(userId).map(user -> {
            team.setUser(user);
            return teamRepository.save(team);
        }).orElseThrow(() -> new UserDidNotExistException("User with id '" + userId + "' did not exist"));
    }

    @Override
    public Team findTeamByIdAndUserId(Integer id, String userId) throws ResourceNotFoundException {
        return teamRepository.findByIdAndUserUserId(id, userId).orElseThrow(
                () -> new ResourceNotFoundException("Team not found with id " + id)
        );
    }

    @Override
    public void deleteTeam(Integer id, String userId) throws ResourceNotFoundException {
        Team team = teamRepository.findByIdAndUserUserId(id, userId).orElseThrow(
                () -> new ResourceNotFoundException("Team not found with id " + id)
        );

        teamRepository.delete(team);
    }
}
