package com.pokedex.project.auth.controller;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pokedex.project.auth.form.LoginForm;
import com.pokedex.project.auth.model.User;
import com.pokedex.project.auth.service.AuthService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthController.class)
class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthService authService;

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void registerUser_ShouldReturnCreatedUser() throws Exception {

        User user = new User();
        user.setUserId("userid");
        user.setUserName("username");
        user.setUserPassword("userpassword");

        Mockito.when(authService.registerUser(any(User.class))).thenReturn(user);

        mockMvc.perform(post("/pokedex/api/v1/auth/register")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(user)))
            .andExpect(jsonPath("$.userId", is(user.getUserId())))
            .andExpect(status().isCreated());
    }

    @Test
    void authenticateUser_ShouldReturnExistingUser() throws Exception {

        LoginForm loginForm = new LoginForm();
        loginForm.setUserId("userid");
        loginForm.setUserPassword("userpassword");

        User user = new User();
        user.setUserId("userid");
        user.setUserName("username");
        user.setUserPassword("userpassword");

        Mockito.when(authService.authenticateUser(loginForm.getUserId(),
                    loginForm.getUserPassword())).thenReturn(user);

        mockMvc.perform(post("/pokedex/api/v1/auth/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(loginForm)))
            .andDo(print())
            .andExpect(jsonPath("$.userId", is(user.getUserId())))
            .andExpect(status().isOk());
    }
}
