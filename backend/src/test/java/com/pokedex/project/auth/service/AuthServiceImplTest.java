package com.pokedex.project.auth.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import com.pokedex.project.auth.model.User;
import com.pokedex.project.auth.repository.UserRepository;
import com.pokedex.project.exception.AuthenticationFailedException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.exception.UserIdAlreadyExistException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class AuthServiceImplTest {

    @InjectMocks
    private AuthServiceImpl authService;

    @Mock
    private UserRepository userRepository;

    @Test
    void registerUser_ShouldReturnRegisteredUserIfNotExist() throws UserIdAlreadyExistException {
        User user = new User();
        user.setUserId("userId");
        user.setUserName("userName");
        user.setUserPassword("userPassword");

        Mockito.when(userRepository.save(user)).thenReturn(user);

        User registeredUser = authService.registerUser(user);

        assertThat(registeredUser).isEqualTo(user);
        Mockito.verify(userRepository).save(any(User.class));
    }

    @Test
    void registerUser_ShouldThrowUserIdAlreadyExistExceptionIfUserIdAlreadyExist() {
        User user = new User();
        user.setUserId("userId");
        user.setUserName("userName");
        user.setUserPassword("userPassword");

        Mockito.when(userRepository.findByUserId("userId")).thenReturn(Optional.of(user));

        Exception exception = assertThrows(
                UserIdAlreadyExistException.class,
                () -> authService.registerUser(user)
        );

        assertTrue(exception.getMessage().contains("already exist"));
    }

    @Test
    void authenticateUser_ShouldReturnUserIfMatchingUserIdAndPasswordExist()
            throws UserDidNotExistException, AuthenticationFailedException {
        User user = new User();
        user.setUserId("userId");
        user.setUserName("userName");
        user.setUserPassword("userPassword");

        Mockito.when(userRepository.findByUserId("userId")).thenReturn(Optional.of(user));

        User expected = authService.authenticateUser("userId", "userPassword");

        assertThat(expected).isEqualTo(user);
    }

    @Test
    void authenticateUser_ShouldThrowUserDidNotExistExceptionIfUserIdDidNotExist() {
        Exception exception = assertThrows(
                UserDidNotExistException.class,
                () -> authService.authenticateUser("userId", "userPassword")
                );

        assertTrue(exception.getMessage().contains("did not exist"));
    }

    @Test
    void authenticateUser_ShouldThrowAuthenticationFailedExceptionIfPasswordDidNotMatch() {
        User user = new User();
        user.setUserId("userId");
        user.setUserName("userName");
        user.setUserPassword("userPassword");

        Mockito.when(userRepository.findByUserId("userId")).thenReturn(Optional.of(user));

        Exception exception = assertThrows(
                AuthenticationFailedException.class,
                () -> authService.authenticateUser("userId", "serPassword")
                );

        assertTrue(exception.getMessage().contains("doesn't match"));
    }
}

