package com.pokedex.project.exception;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pokedex.project.auth.controller.AuthController;
import com.pokedex.project.auth.model.User;
import com.pokedex.project.auth.service.AuthService;
import com.pokedex.project.pokemonlist.controller.PokemonController;
import com.pokedex.project.pokemonlist.model.Pokemon;
import com.pokedex.project.pokemonlist.service.PokemonService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@RunWith(SpringRunner.class)
@WebMvcTest({PokemonController.class, AuthController.class})
class CustomExceptionHandlerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PokemonService pokemonService;

    @MockBean
    private AuthService authService;

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void pokemonController_ShouldUseCustomExceptionHandler_HandleAllException() throws Exception {
        Pokemon pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");

        Mockito.when(pokemonService.createPokemon(any(Pokemon.class)))
                .thenThrow(IllegalArgumentException.class);

        mvc.perform(MockMvcRequestBuilders.post("/pokedex/api/v1/pokemon/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(pokemon)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").value("Server Error"));
    }

    @Test
    void pokemonController_ShouldUseCustomExceptionHandler_HandleResourceNotFoundException()
            throws Exception {
        final Integer id = 1;

        Mockito.when(pokemonService.findPokemonById(id))
                .thenThrow(ResourceNotFoundException.class);

        mvc.perform( MockMvcRequestBuilders.get("/pokedex/api/v1/pokemon/{id}", id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Resource Not Found"));
    }

    @Test
    void pokemonController_ShouldUseCustomExceptionHandler_HandleAuthenticationFailedException()
            throws Exception {
        final String userId = "userId";
        final String userPassword = "userPassword";

        Mockito.when(authService.authenticateUser(userId, userPassword))
                .thenThrow(AuthenticationFailedException.class);

        mvc.perform( MockMvcRequestBuilders.post("/pokedex/api/v1/auth/authenticate/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"userId\":\"userId\",\"userPassword\":\"userPassword\"}"))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.message").value("Authentication Failed"));
    }

    @Test
    void pokemonController_ShouldUseCustomExceptionHandler_HandleUserDidNotExistException()
            throws Exception {
        final String userId = "userId";
        final String userPassword = "userPassword";

        Mockito.when(authService.authenticateUser(userId, userPassword))
                .thenThrow(UserDidNotExistException.class);

        mvc.perform( MockMvcRequestBuilders.post("/pokedex/api/v1/auth/authenticate/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"userId\":\"userId\",\"userPassword\":\"userPassword\"}"))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.message").value("User Didn't Exist"));
    }

    @Test
    void pokemonController_ShouldUseCustomExceptionHandler_HandleUserIdAlreadyExistException()
            throws Exception {

        Mockito.when(authService.registerUser(any(User.class)))
                .thenThrow(UserIdAlreadyExistException.class);

        mvc.perform( MockMvcRequestBuilders.post("/pokedex/api/v1/auth/register/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(
                    "{\"userId\":\"userId\","
                    + "\"userPassword\":\"userPassword\","
                    + "\"userName\":\"userName\"}"))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.message").value("User ID Already Exist"));
    }

}
