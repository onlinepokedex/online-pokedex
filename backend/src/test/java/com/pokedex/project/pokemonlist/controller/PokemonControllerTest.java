package com.pokedex.project.pokemonlist.controller;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pokedex.project.pokemonlist.model.Pokemon;
import com.pokedex.project.pokemonlist.service.PokemonService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;


@RunWith(SpringRunner.class)
@WebMvcTest(PokemonController.class)
class PokemonControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PokemonService pokemonService;

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void getAllPokemon_ShouldReturnListOfPokemonWithJsonFormat() throws Exception {
        Pokemon pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");

        List<Pokemon> pokemonList = Collections.singletonList(pokemon);

        Mockito.when(pokemonService.findAllPokemon()).thenReturn(pokemonList);

        mvc.perform(MockMvcRequestBuilders.get("/pokedex/api/v1/pokemon/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(pokemon.getName())));
    }

    @Test
    void createPokemon_ShouldSaveAndReturnPokemonWithJsonFormat() throws Exception {
        Pokemon pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");

        Mockito.when(pokemonService.createPokemon(any(Pokemon.class))).thenReturn(pokemon);

        mvc.perform(MockMvcRequestBuilders.post("/pokedex/api/v1/pokemon/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(pokemon)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(pokemon.getName())));
    }

    @Test
    void getPokemonById_ShouldReturnPokemonWithGivenIdWithJsonFormat() throws Exception {
        final Integer id = 1;

        Pokemon pokemon = new Pokemon("Bulbasaur", null, null, null,
                null, null, null, null, null,
                null, null, null, null, null, null);
        pokemon.setId(id);

        Mockito.when(pokemonService.findPokemonById(id)).thenReturn(pokemon);

        mvc.perform( MockMvcRequestBuilders.get("/pokedex/api/v1/pokemon/{id}", id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(pokemon.getId()));
    }

    @Test
    void deletePokemonById_ShouldDeletePokemonWithGivenId() throws Exception {
        final Integer id = 1;

        Mockito.doNothing().when(pokemonService).deletePokemon(id);

        mvc.perform(MockMvcRequestBuilders.delete("/pokedex/api/v1/pokemon/{id}", id))
                .andExpect(status().isNoContent());

        Mockito.verify(pokemonService, times(1)).deletePokemon(id);
        Mockito.verifyNoMoreInteractions(pokemonService);
    }
}