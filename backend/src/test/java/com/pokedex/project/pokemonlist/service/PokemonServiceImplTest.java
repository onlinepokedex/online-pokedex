package com.pokedex.project.pokemonlist.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.pokemonlist.model.Pokemon;
import com.pokedex.project.pokemonlist.repository.PokemonRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class PokemonServiceImplTest {

    @InjectMocks
    private PokemonServiceImpl pokemonService;

    @Mock
    private PokemonRepository pokemonRepository;

    @Test
    void findAllPokemon_ShouldReturnListOfPokemon() {
        Pokemon bulbasaur = new Pokemon();
        bulbasaur.setName("Bulbasaur");

        Pokemon squirtle = new Pokemon();
        squirtle.setName("Squirtle");

        List<Pokemon> expected = new ArrayList<>();
        expected.add(bulbasaur);
        expected.add(squirtle);

        Mockito.when(pokemonRepository.findAll()).thenReturn(expected);

        List<Pokemon> actual = pokemonService.findAllPokemon();

        assertThat(actual.size()).isEqualTo(2);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void findPokemonById_WithValidId_ShouldReturnPokemonWithGivenId()
            throws ResourceNotFoundException {
        final Integer id = 1;

        Pokemon pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");

        Mockito.when(pokemonRepository.findById(id)).thenReturn(Optional.of(pokemon));

        Pokemon expected = pokemonService.findPokemonById(id);

        assertThat(expected).isEqualTo(pokemon);

    }

    @Test
    void findPokemonById_WithInvalidId_ShouldThrowResourceNotFoundException() {
        Exception exception = assertThrows(
                ResourceNotFoundException.class,
                () -> pokemonService.findPokemonById(1)
        );

        assertTrue(exception.getMessage().contains("not found"));
    }

    @Test
    void createPokemon_ShouldSaveAndReturnNewPokemon() {
        Pokemon pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");

        Mockito.when(pokemonRepository.save(pokemon))
                .thenAnswer(invocation -> invocation.getArgument(0));

        Pokemon savedPokemon =  pokemonService.createPokemon(pokemon);

        assertThat(savedPokemon).isEqualTo(pokemon);
        Mockito.verify(pokemonRepository).save(any(Pokemon.class));
    }

    @Test
    void deletePokemon_WithValidId_ShouldDeletePokemonWithGivenId()
            throws ResourceNotFoundException {
        final Integer id = 1;

        Pokemon pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");

        Mockito.when(pokemonRepository.findById(id)).thenReturn(Optional.of(pokemon));

        pokemonService.deletePokemon(id);

        Mockito.verify(pokemonRepository, Mockito.times(1)).delete(pokemon);
    }

    @Test
    void deletePokemon_WithInvalidId_ShouldThrowResourceNotFoundException() {
        Exception exception = assertThrows(
                ResourceNotFoundException.class,
                () -> pokemonService.deletePokemon(1)
        );

        assertTrue(exception.getMessage().contains("not found"));


    }
}