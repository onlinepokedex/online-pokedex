package com.pokedex.project.teambuilder.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pokedex.project.auth.model.User;
import com.pokedex.project.teambuilder.model.Team;
import com.pokedex.project.teambuilder.service.TeamService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamController.class)
class TeamControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeamService teamService;

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void getAllTeamByUserId_ShouldReturnListOfTeamsWithJsonFormat() throws Exception {
        Team team = new Team();
        User user = new User();
        user.setUserId("admin");
        user.setUserPassword("This is password");
        team.setName("Coba-coba team");
        team.setUser(user);

        List<Team> teamList = Collections.singletonList(team);

        Mockito.when(teamService.findAllByUserId(user.getUserId())).thenReturn(teamList);

        mvc.perform(MockMvcRequestBuilders.get("/pokedex/api/v1/team/admin/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(team.getName())));
    }

    @Test
    void createTeam_ShouldSaveAndReturnTeamWithJsonFormat() throws Exception {
        Team team = new Team();
        User user = new User();
        user.setUserId("admin");
        user.setUserPassword("This is password");
        team.setName("Coba-coba team");
        team.setUser(user);

        Mockito.when(teamService.createTeam(any(Team.class), eq(user.getUserId()))).thenReturn(team);

        mvc.perform(MockMvcRequestBuilders.post("/pokedex/api/v1/team/admin/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(team)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(team.getName())));
    }

    @Test
    void getTeamByIdAndUserId_ShouldReturnTeamWithGivenIdWithJsonFormat() throws Exception {
        final Integer id = 1;

        Team team = new Team("Coba-coba team", 1, 2, 3, 4, 5, 6);
        team.setId(id);

        User user = new User();
        user.setUserId("admin");
        user.setUserPassword("This is password");
        team.setUser(user);

        Mockito.when(teamService.findTeamByIdAndUserId(id, user.getUserId())).thenReturn(team);

        mvc.perform( MockMvcRequestBuilders.get("/pokedex/api/v1/team/{user}/{id}", user.getUserId(), id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(team.getId()));
    }

    @Test
    void deleteTeamByIdAndUserId_ShouldDeleteTeamWithGivenId() throws Exception {
        final Integer id = 1;
        final String userId = "admin";

        Mockito.doNothing().when(teamService).deleteTeam(id,userId);

        mvc.perform(MockMvcRequestBuilders.delete("/pokedex/api/v1/team/{user}/{id}", userId, id))
                .andExpect(status().isNoContent());

        Mockito.verify(teamService, times(1)).deleteTeam(id, userId);
        Mockito.verifyNoMoreInteractions(teamService);
    }
}