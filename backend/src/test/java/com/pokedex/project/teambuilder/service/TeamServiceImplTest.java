package com.pokedex.project.teambuilder.service;

import com.pokedex.project.auth.model.User;
import com.pokedex.project.auth.repository.UserRepository;
import com.pokedex.project.exception.ResourceNotFoundException;
import com.pokedex.project.exception.UserDidNotExistException;
import com.pokedex.project.teambuilder.model.Team;
import com.pokedex.project.teambuilder.repository.TeamRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class TeamServiceImplTest {

    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private TeamRepository teamRepository;

    @Mock
    private UserRepository userRepository;

    @Test
    void findAllByUserId_ShouldReturnListOfTeam() {
        Team firstTeam = new Team();
        firstTeam.setName("First Team");

        Team secondTeam = new Team();
        secondTeam.setName("Second Team");

        User user = new User();
        user.setUserId("admin");

        firstTeam.setUser(user);
        secondTeam.setUser(user);

        List<Team> expected = new ArrayList<>();
        expected.add(firstTeam);
        expected.add(secondTeam);

        Mockito.when(teamRepository.findByUserUserId(user.getUserId())).thenReturn(expected);

        List<Team> actual = teamService.findAllByUserId(firstTeam.getUser().getUserId());

        assertThat(actual.size()).isEqualTo(2);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void findTeamByIdAndUserId_WithValidId_ShouldReturnTeamWithGivenId()
            throws ResourceNotFoundException {
        final Integer id = 1;

        Team firstTeam = new Team();
        firstTeam.setName("First Team");
        User user = new User();
        user.setUserId("admin");
        firstTeam.setUser(user);

        Mockito.when(teamRepository.findByIdAndUserUserId(id, user.getUserId())).thenReturn(Optional.of(firstTeam));

        Team expected = teamService.findTeamByIdAndUserId(id, user.getUserId());

        assertThat(expected).isEqualTo(firstTeam);

    }
//
    @Test
    void findTeamByIdAndUserId_WithInvalidId_ShouldThrowResourceNotFoundException() {
        Exception exception = assertThrows(
                ResourceNotFoundException.class,
                () -> teamService.findTeamByIdAndUserId(1, "admin")
        );

        assertTrue(exception.getMessage().contains("not found"));
    }

    @Test
    void createTeam_ShouldSaveAndReturnNewTeam() throws UserDidNotExistException {
        Team firstTeam = new Team();
        firstTeam.setName("First Team");
        User user = new User();
        user.setUserId("admin");
        firstTeam.setUser(user);

        Mockito.when(userRepository.findByUserId(user.getUserId()))
                .thenReturn(Optional.of(user));

        Mockito.when(teamRepository.save(firstTeam))
                .thenAnswer(invocation -> invocation.getArgument(0));

        Team savedTeam =  teamService.createTeam(firstTeam, user.getUserId());

        assertThat(savedTeam).isEqualTo(firstTeam);
        Mockito.verify(teamRepository).save(any(Team.class));
    }

    @Test
    void createTeam_WithInvalidUserId_ShouldSaveAndReturnNewTeam()
            throws UserDidNotExistException {
        Team firstTeam = new Team();
        firstTeam.setName("First Team");
        User user = new User();
        user.setUserId("admin");
        firstTeam.setUser(user);

        Exception exception = assertThrows(
                UserDidNotExistException.class,
                () -> teamService.createTeam(firstTeam, user.getUserId())
        );

        assertTrue(exception.getMessage().contains("not exist"));
    }

    @Test
    void deleteTeam_WithValidId_ShouldDeleteTeamWithGivenId()
            throws ResourceNotFoundException {
        final Integer id = 1;

        Team firstTeam = new Team();
        firstTeam.setName("First Team");
        User user = new User();
        user.setUserId("admin");
        firstTeam.setUser(user);

        Mockito.when(teamRepository.findByIdAndUserUserId(id, user.getUserId())).thenReturn(Optional.of(firstTeam));

        teamService.deleteTeam(id, user.getUserId());

        Mockito.verify(teamRepository, Mockito.times(1)).delete(firstTeam);
    }

    @Test
    void deletePokemon_WithInvalidId_ShouldThrowResourceNotFoundException() {
        Exception exception = assertThrows(
                ResourceNotFoundException.class,
                () -> teamService.deleteTeam(1, "admin")
        );

        assertTrue(exception.getMessage().contains("not found"));


    }

}