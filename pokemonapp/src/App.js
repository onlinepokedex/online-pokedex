import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import NavBar from './components/layout/NavBar';
import Dashboard from "./components/layout/Dashboard";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Pokemon from './components/pokemon/Pokemon';
import Registration from './components/authentication/Registration';
import Login from './components/authentication/Login'
import Builder from './components/Builder/builder'
import backgroundImg from './patternpoke.png';
import Team from './components/Builder/team';
import TeamList from './components/Builder/team-list';


class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div className="App" style={{ background: `url(${backgroundImg}` }}>
          <NavBar />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/pokemon/:pokemonIndex" component={Pokemon} />
              <Route exact path="/register" component={Registration} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/builder" component={Builder} />
              <Route exact path="/builder/team" component={TeamList} />
              <Route exact path="/builder/team/:teamId" component={Team} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>

    );
  }
}

export default App;
