import React from 'react';
import { mount, shallow} from 'enzyme';
import { MemoryRouter } from 'react-router';
import { HashRouter } from 'react-router-dom';
import Dashboard from "./components/layout/Dashboard";
import App from './App';
import { configure } from 'enzyme';
import ReactDOM from 'react-dom';
import Adapter from 'enzyme-adapter-react-16';
import PokemonList from "./components/pokemon/PokemonList";
import PokemonCard from "./components/pokemon/PokemonCard";
import Pokemon from "./components/pokemon/Pokemon";
import Login from './components/authentication/Login';
import Registration from './components/authentication/Registration';
import Builder from './components/Builder/builder';
import {CustomAuthContext} from './custom-auth';
import AuthBar from './components/layout/AuthBar';
configure({ adapter: new Adapter() });


describe('App', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App />, div);
    });
    it('should show Dashboard component for / router (using memory router)', () => {
        const component = mount(
            <MemoryRouter initialEntries={['/']} >
                <App />
            </MemoryRouter>
        );
        expect(component.find(Dashboard)).toHaveLength(1);
    });
});


describe("PokemonList", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<PokemonList />, div);
    });
    it('should show 12 visiable pokemon', ()=> {
        const component = shallow(<PokemonList />);
        expect(component.instance().state.visiable).toEqual(12);
    });
    it('should render pokemon card', ()=> {
        const component = shallow(<PokemonList />);
        expect(component.instance().state.pokemon).not.toBeNull();
    });

});


describe("PokemonCard", () => {
    it('renders PokemonCard', () => {
        expect(PokemonCard).toBeDefined();
        const tree = shallow(<PokemonCard />);
        expect(tree).not.toBeNull()
    });
    it('should show Pokemon page for / router (using memory router)', () => {
        const component = mount(
            <MemoryRouter initialEntries={['/']} >
                <Pokemon />
            </MemoryRouter>
        );
        expect(component.find(Pokemon)).toHaveLength(1);
    });
});


describe("Pokemon", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Pokemon/>, div);
    });
    it('renders card', () => {
        const editor = mount(<Pokemon/>);
        expect(editor.find('div.card')).toHaveLength(1);
    });
    it('renders progress bar', () => {
        const editor = mount(<Pokemon/>);
        expect(editor.find('div.progress')).toHaveLength(6);
    });
    it('renders pokemon image', () => {
        const editor = mount(<Pokemon/>);
        expect(editor.find('img')).toHaveLength(1);
    });
});

describe("AuthBar", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(
            <HashRouter><AuthBar /></HashRouter>,
            div);
    });
    it('renders login link', () => {
        const editor = mount(
            <HashRouter><AuthBar /></HashRouter>
        );
        expect(editor.find('Link').findWhere(n => n.prop('to') === '/login')).toHaveLength(1);
    });
    it('renders register link', () => {
        const editor = mount(
            <HashRouter><AuthBar /></HashRouter>
        );
        expect(editor.find('Link').findWhere(n => n.prop('to') === '/register')).toHaveLength(1);
    });
    it('renders logout when user signed in', () => {
        const editor = mount(
            <CustomAuthContext.Provider value={{user : 'user'}}>
            <HashRouter><AuthBar /></HashRouter>
            </CustomAuthContext.Provider>
        );
        expect(editor.text()).toEqual(
            expect.stringContaining('Log Out')
        );
    });
});

describe("Login", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Login />, div);
    });
    it('renders login forms', () => {
        const editor = mount(<Login />);
        expect(editor.find('form')).toHaveLength(1);
    });
    it('renders required input', () => {
        const editor = mount(<Login />);
        expect(editor.find('input')).toHaveLength(3);
    });
    it('redirect if user logged in', () => {
        const editor = mount(
            <CustomAuthContext.Provider value={{user : 'user'}}>
            <HashRouter>
                <Login />
            </HashRouter>
            </CustomAuthContext.Provider>);
        expect(editor.find('Redirect')).toHaveLength(1);
    });
});

describe("Registration", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Registration />, div);
    });
    it('renders registration forms', () => {
        const editor = mount(<Registration />);
        expect(editor.find('form')).toHaveLength(1);
    });
    it('renders required input', () => {
        const editor = mount(<Registration />);
        expect(editor.find('input')).toHaveLength(4);
    });
});

describe("Builder", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Builder />, div);
    });
});


