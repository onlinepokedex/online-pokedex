import React, { Component } from "react";
import FetchData from "./fetch-data";
import { Form, Row, Col, Table, Button } from "react-bootstrap";
import WeaknessMultiplier from "./weakness-multiplier"
import { CustomAuthContext } from '../../custom-auth';
import Axios from "axios";

export default class Builder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemon: [],
      team: [],
      totalWeakness: 0,
      totalResistance: 0,
      selectPokemon: () => { }
    };
  }

  /* istanbul ignore next */
  async componentDidMount() {
    await this.getPokemon();
  }

  /* istanbul ignore next */
  async getPokemon() {
    const listPokemon = await FetchData(
      "https://online-pokedex-api.herokuapp.com/pokedex/api/v1/pokemon/"
    );
    this.setState({
      isLoading: false,
      pokemon: listPokemon,
    });
  }
  /* istanbul ignore next */
  selectPokemon = e => {
    const slot = e.target.name - 1;
    const value = e.target.value - 1;

    if (value === -1) return;

    const myTeam = this.state.team;
    const pokemon = this.state.pokemon[value]
    myTeam[slot] = pokemon;
    console.log("Selected: ", pokemon.name);
    console.log(myTeam);
    this.setState({ team: myTeam });

    this.calculateTotal();

  };

  /* istanbul ignore next */
  calculateTotal = () => {
    this.setState({
      totalResistance: 0,
      totalWeakness: 0
    });

    let weakness = 0;
    let resistance = 0;

    for (let i = 0; i < this.state.team.length; i++) {
      const pokemon = this.state.team[i];
      if (pokemon) {
        Object.keys(pokemon.weakness_multiplier).forEach((key, index) => {
          if (pokemon.weakness_multiplier[key] > 1.0) {
            weakness++;
          } else if (pokemon.weakness_multiplier[key] < 1.0) {
            resistance++;
          }
        });
      }
    }

    this.setState({
      totalResistance: resistance,
      totalWeakness: weakness
    });
  }

  /* istanbul ignore next */
  saveClicked = () => {
    const { user } = this.context || {}
    if (user) {
      const enteredName = prompt("Please enter your team name");
      this.postData(user, enteredName);
    } else {
      this.props.history.push('/login')
    }
  }

  /* istanbul ignore next */
  async postData(user, name) {
    await Axios.post("https://online-pokedex-api.herokuapp.com/pokedex/api/v1/team/" + user.userId, {
      name: name,
      first_pokemon: this.state.team[0].id,
      second_pokemon: this.state.team[1].id,
      third_pokemon: this.state.team[2].id,
      fourth_pokemon: this.state.team[3].id,
      fifth_pokemon: this.state.team[4].id,
      sixth_pokemon: this.state.team[5].id,
    }).then(response => {
      console.log(response);
      this.props.history.push({
        pathname: '/builder/team/' + response.data.id,
        state: {
          myTeam: this.state.myTeam,
          name: name,
          pokemon: this.state.pokemon,
        }
      });
    }).catch(error => {
      console.log(error)
    })
  }

  /* istanbul ignore next */
  render() {
    const first = this.state.team[0]
    const second = this.state.team[1]
    const third = this.state.team[2]
    const fourth = this.state.team[3]
    const fifth = this.state.team[4]
    const sixth = this.state.team[5]

    return (
      <div className="container">
        <div
          className="inputPokemon"
          style={{
            backgroundColor: "white",
            marginTop: "20px",
            padding: "10px"
          }}>
          <h1 style={{ textAlign: "center", marginBottom: "25px" }}>Welcome to Pokemon Team Builder!</h1>
          <Form>
            <Form.Group as={Row} controlId="pokemon1">
              <Form.Label column sm={8}>
                Input Pokemon 1
            </Form.Label>
              <Col sm={12}>
                <Form.Control as="select" name={1} onChange={this.selectPokemon}>
                  <option value={0} key={0} >Enter pokemon name</option>
                  {this.state.pokemon.map(pokemon => (
                    <option value={pokemon.id} key={pokemon.id}>{pokemon.name}</option>
                  ))}
                </Form.Control>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="pokemon2">
              <Form.Label column sm={8}>
                Input Pokemon 2
            </Form.Label>
              <Col sm={12}>
                <Form.Control as="select" name={2} onChange={this.selectPokemon}>
                  <option value={0} key={0} >Enter pokemon name</option>
                  {this.state.pokemon.map(pokemon => (
                    <option value={pokemon.id} key={pokemon.id}>{pokemon.name}</option>
                  ))}
                </Form.Control>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="pokemon3">
              <Form.Label column sm={8}>
                Input Pokemon 3
            </Form.Label>
              <Col sm={12}>
                <Form.Control as="select" name={3} onChange={this.selectPokemon}>
                  <option value={0} key={0} >Enter pokemon name</option>
                  {this.state.pokemon.map(pokemon => (
                    <option value={pokemon.id} key={pokemon.id}>{pokemon.name}</option>
                  ))}
                </Form.Control>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="pokemon4">
              <Form.Label column sm={8}>
                Input Pokemon 4
            </Form.Label>
              <Col sm={12}>
                <Form.Control as="select" name={4} onChange={this.selectPokemon}>
                  <option value={0} key={0} >Enter pokemon name</option>
                  {this.state.pokemon.map(pokemon => (
                    <option value={pokemon.id} key={pokemon.id}>{pokemon.name}</option>
                  ))}
                </Form.Control>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="pokemon5">
              <Form.Label column sm={8}>
                Input Pokemon 5
            </Form.Label>
              <Col sm={12}>
                <Form.Control as="select" name={5} onChange={this.selectPokemon}>
                  <option value={0} key={0} >Enter pokemon name</option>
                  {this.state.pokemon.map(pokemon => (
                    <option value={pokemon.id} key={pokemon.id}>{pokemon.name}</option>
                  ))}
                </Form.Control>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="pokemon6">
              <Form.Label column sm={8}>
                Input Pokemon 6
            </Form.Label>
              <Col sm={12}>
                <Form.Control as="select" name={6} onChange={this.selectPokemon}>
                  <option value={0} key={0} >Enter pokemon name</option>
                  {this.state.pokemon.map(pokemon => (
                    <option value={pokemon.id} key={pokemon.id}>{pokemon.name}</option>
                  ))}
                </Form.Control>
              </Col>
            </Form.Group>

          </Form>

        </div >
        <br></br>
        <div className="tableWeakness" style={{ backgroundColor: "white", padding: "10px" }}>
          <Table hover bordered responsive="xl" style={{ textAlign: "center" }}>
            <thead>
              <tr>
                <th>Type</th>
                <th>{first ? first.name : ''}</th>
                <th>{second ? second.name : ''}</th>
                <th>{third ? third.name : ''}</th>
                <th>{fourth ? fourth.name : ''}</th>
                <th>{fifth ? fifth.name : ''}</th>
                <th>{sixth ? sixth.name : ''}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#B1C12E", width: "10%", color: "white" }}>BUG</th>
                <WeaknessMultiplier pokemon={first} type="bug"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="bug"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="bug"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="bug"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="bug"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="bug"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#4F3A2D", width: "10%", color: "white" }}>DARK</th>
                <WeaknessMultiplier pokemon={first} type="dark"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="dark"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="dark"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="dark"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="dark"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="dark"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#755EDF", width: "10%", color: "white" }}>DRAGON</th>
                <WeaknessMultiplier pokemon={first} type="dragon"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="dragon"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="dragon"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="dragon"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="dragon"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="dragon"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#FCBC17", width: "10%", color: "white" }}>ELECTRIC</th>
                <WeaknessMultiplier pokemon={first} type="electric"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="electric"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="electric"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="electric"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="electric"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="electric"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#82351D", width: "10%", color: "white" }}>FIGHTING</th>
                <WeaknessMultiplier pokemon={first} type="fighting"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="fighting"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="fighting"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="fighting"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="fighting"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="fighting"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#E73B0C", width: "10%", color: "white" }}>FIRE</th>
                <WeaknessMultiplier pokemon={first} type="fire"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="fire"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="fire"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="fire"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="fire"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="fire"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#A3B3F7", width: "10%", color: "white" }}>FLYING</th>
                <WeaknessMultiplier pokemon={first} type="flying"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="flying"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="flying"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="flying"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="flying"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="flying"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#6060B2", width: "10%", color: "white" }}>GHOST</th>
                <WeaknessMultiplier pokemon={first} type="ghost"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="ghost"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="ghost"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="ghost"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="ghost"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="ghost"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#74C236", width: "10%", color: "white" }}>GRASS</th>
                <WeaknessMultiplier pokemon={first} type="grass"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="grass"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="grass"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="grass"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="grass"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="grass"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#D3B357", width: "10%", color: "white" }}>GROUND</th>
                <WeaknessMultiplier pokemon={first} type="ground"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="ground"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="ground"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="ground"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="ground"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="ground"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#A3E7FD", width: "10%", color: "white" }}>ICE</th>
                <WeaknessMultiplier pokemon={first} type="ice"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="ice"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="ice"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="ice"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="ice"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="ice"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#C8C4BC", width: "10%", color: "white" }}>NORMAL</th>
                <WeaknessMultiplier pokemon={first} type="normal"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="normal"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="normal"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="normal"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="normal"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="normal"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#934594", width: "10%", color: "white" }}>POISON</th>
                <WeaknessMultiplier pokemon={first} type="poison"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="poison"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="poison"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="poison"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="poison"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="poison"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#ED4882", width: "10%", color: "white" }}>PSYCHIC</th>
                <WeaknessMultiplier pokemon={first} type="psychic"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="psychic"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="psychic"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="psychic"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="psychic"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="psychic"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#B9A156", width: "10%", color: "white" }}>ROCK</th>
                <WeaknessMultiplier pokemon={first} type="rock"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="rock"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="rock"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="rock"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="rock"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="rock"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#B5B5C3", width: "10%", color: "white" }}>STEEL</th>
                <WeaknessMultiplier pokemon={first} type="steel"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="steel"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="steel"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="steel"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="steel"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="steel"></WeaknessMultiplier>
              </tr>
            </tbody>
            <tbody>
              <tr>
                <th style={{ backgroundColor: "#3295F6", width: "10%", color: "white" }}>WATER</th>
                <WeaknessMultiplier pokemon={first} type="water"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={second} type="water"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={third} type="water"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fourth} type="water"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={fifth} type="water"></WeaknessMultiplier>
                <WeaknessMultiplier pokemon={sixth} type="water"></WeaknessMultiplier>
              </tr>
            </tbody>
          </Table>
          <br></br>
          <h3>Total Weakness: {this.state.totalWeakness}</h3>
          <h3>Total Resistance: {this.state.totalResistance} </h3>
        </div>
        <br></br>
        <Button style={{ marginBottom: "15px" }} variant="primary" size="lg" block onClick={this.saveClicked}>Save</Button>
      </div>
    );
  }

}

Builder.contextType = CustomAuthContext;

