import React, { Component } from "react";
import FetchData from "./fetch-data";
import { ListGroup } from "react-bootstrap";
import { CustomAuthContext } from '../../custom-auth';
import { Link } from "react-router-dom";

export default class TeamList extends Component {
    /* istanbul ignore next */
    constructor(props) {
        super(props);
        this.state = {
            pokemon: [],
            teams: [],
        };
    }

    /* istanbul ignore next */
    async componentDidMount() {
        await this.getPokemon();
        await this.getTeamList();
    }

    /* istanbul ignore next */
    async getPokemon() {
        const listPokemon = await FetchData(
            "https://online-pokedex-api.herokuapp.com/pokedex/api/v1/pokemon/"
        );
        this.setState({
            pokemon: listPokemon,
        });
        console.log(listPokemon)
    }

    /* istanbul ignore next */
    async getTeamList() {
        const { user } = this.context || {}
        const teamList = await FetchData(
            "https://online-pokedex-api.herokuapp.com/pokedex/api/v1/team/" + user.userId
        );
        this.setState({
            teams: teamList,
        });
    }

    /* istanbul ignore next */
    render() {
        return (
            <div className="container">
                <ListGroup>
                    {this.state.teams.map(team =>
                        <ListGroup.Item><Link to={{
                            pathname: '/builder/team/' + team.id,
                            state: { pokemon: this.state.pokemon }
                        }} >{team.name}</Link></ListGroup.Item>
                    )}
                </ListGroup>
            </div>
        );
    }

}

TeamList.contextType = CustomAuthContext;

