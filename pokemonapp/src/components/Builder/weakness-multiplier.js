import React, { Component } from "react";

export default class WeaknessMultiplier extends Component {
    /* istanbul ignore next */
    render() {
        return <td style={this.weaknessStyling(this.props.pokemon, this.props.type)}>
            {this.props.pokemon ? this.props.pokemon.weakness_multiplier[this.props.type] + "x" : ''}
        </td >

    }
    weaknessStyling = (pokemon, type) => {
        /* istanbul ignore next */
        if (pokemon) {
            if (pokemon.weakness_multiplier[type] > 1.0) {
                return { backgroundColor: 'red' }
            } else if (pokemon.weakness_multiplier[type] < 1.0) {
                return { backgroundColor: '#90EE90' }
            }
        }
    }
}


