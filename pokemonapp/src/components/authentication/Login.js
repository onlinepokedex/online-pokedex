import React, { Component } from 'react';
import axios from 'axios';
import { CustomAuthContext } from '../../custom-auth';
import { Redirect } from 'react-router-dom';
import './UserForm.css';

export default class Login extends Component {
  /* istanbul ignore next */
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      userPassword: '',
    };

    this.handleChange = this.handleChange.bind(this)
    this.loginUser = this.loginUser.bind(this)
  }
  /* istanbul ignore next */
  handleChange(event) {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    });
  }
  /* istanbul ignore next */
  loginUser(event) {

    const { setUser } = this.context;

    axios.post(process.env.REACT_APP_SERVER_HOST_API + 'auth/authenticate', {
      userId: this.state.userId,
      userPassword: this.state.userPassword,
    }).then(res => {
      setUser(res.data);
    }).catch(err => {

      const { message, details } = err.response.data;
      console.log(message);
      this.setState({
        resMessage: 'Login Failed',
        resDetails: details[0],
      })
    });
    event.preventDefault();
  }

  render() {

    const { user } = this.context || {};
    /* istanbul ignore next */
    if (user) {
      return (
        <Redirect to='/' />
      );
    }
    /* istanbul ignore next */
    return (
      <form onSubmit={this.loginUser} method='post'>
        <table>
          <tbody>
            <tr>
              <td>User ID :</td>
              <td><input type='text' value={this.state.userId} onChange={this.handleChange} id='userId' name='userId' /></td>
            </tr>
            <tr>
              <td>User Password :</td>
              <td><input type='password' value={this.state.userPassword} onChange={this.handleChange} id='userPassword' name='userPassword' /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input type='submit' id='submit' value='Login' /></td>
            </tr>
            <tr>
              <td><div id='message'>{this.state.resMessage || <>&nbsp;</>}</div></td>
              <td><div id='details'>{this.state.resDetails || <>&nbsp;</>}</div></td>
            </tr>
          </tbody>
        </table>
      </form>
    )
  }
}

Login.contextType = CustomAuthContext;

