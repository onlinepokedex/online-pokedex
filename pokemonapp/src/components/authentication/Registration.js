import React, {Component} from 'react';
import axios from 'axios';
import './UserForm.css';

export default class Registration extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userId : '',
      userName : '',
      userPassword : '',
      resMessage : '',
      resDetails : '',
    };

    this.handleChange = this.handleChange.bind(this)
    this.registerUser = this.registerUser.bind(this)
  }

  /* istanbul ignore next */
  handleChange(event) {
    this.setState({
      ...this.state,
      [event.target.name] : event.target.value
    });
  }

  /* istanbul ignore next */
  registerUser(event) {
    axios.post(process.env.REACT_APP_SERVER_HOST_API + 'auth/register', {
      userId : this.state.userId,
      userName : this.state.userName,
      userPassword : this.state.userPassword,
    }).then(res => {
      this.setState({
        userId : '',
        userName : '',
        userPassword : '',
        resMessage : 'Register Success',
        resDetails : undefined,
      })
    }).catch(err => {

      const { message, details } = err.response.data;
      console.log(message);
      this.setState({
        resMessage : 'Register Failed',
        resDetails : details[0],
      })
    });
    event.preventDefault();
  }

  /* istanbul ignore next */
  render() {

    return (
      <form onSubmit={this.registerUser} method='post'>
      <table>
      <tbody>
      <tr>
        <td>User ID :</td>
        <td><input type='text' value={this.state.userId} onChange={this.handleChange} id='userId' name='userId'/></td>
      </tr>
      <tr>
        <td>User Name :</td>
        <td><input type='text' value={this.state.userName} onChange={this.handleChange} id='userName' name='userName'/></td>
      </tr>
      <tr>
        <td>User Password :</td>
        <td><input type='password' value={this.state.userPassword} onChange={this.handleChange} id='userPassword' name='userPassword'/></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type='submit' id='submit' value='Register'/></td>
      </tr>
      <tr>
        <td><div id='message'>{this.state.resMessage || <>&nbsp;</>}</div></td>
        <td><div id='details'>{this.state.resDetails || <>&nbsp;</>}</div></td>
      </tr>
      </tbody>
      </table>
      </form>
    )
  }
}

