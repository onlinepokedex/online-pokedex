import React from 'react';
import { useCustomAuth } from '../../custom-auth';
import { Link } from 'react-router-dom';

const AuthBar = (props) => {

  const { user, logOut } = useCustomAuth() || {};
  /* istanbul ignore next */
  if (user) {
    return (
      <>
        <li><Link to='/builder/team'>My Team</Link></li>
        <li><Link to='/' onClick={e => { logOut() }}>Log Out</Link></li>
      </>
    )
  }
  /* istanbul ignore next */
  return (
    <>
      <li><Link to='/login'>Log In</Link></li>
      <li><Link to='/register' >Register</Link></li>
    </>
  )
}

export default AuthBar;

