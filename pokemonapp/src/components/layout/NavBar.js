import React from 'react';

import '../../App.css';
import AuthBar from './AuthBar';
import { Link } from 'react-router-dom';
// import { ReactComponent as Pokeball } from 'pokeball.svg';

const navbar = props => (
    <header className="navbar fixed-top">
        <nav className="navbar_navigation w-100">
            <div></div>
            <div className="navbar_logo"><a href="/">Online Pokédex</a></div>
            <div className="spacer"></div>
            <div className="navbar_items">
                <ul>
                    <li><Link to='/builder'>Build Team</Link></li>
                    <AuthBar />
                </ul>
            </div>
        </nav>
    </header>
);

export default navbar;


