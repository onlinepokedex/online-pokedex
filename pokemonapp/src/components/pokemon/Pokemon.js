import React, { Component } from 'react';
import axios from "axios";

const TYPE_COLORS = {
    bug: 'B1C12E',
    dark: '4F3A2D',
    dragon: '755EDF',
    electric: 'FCBC17',
    fairy: 'F4B1F4',
    fighting: '823551D',
    fire: 'E73B0C',
    flying: 'A3B3F7',
    ghost: '6060B2',
    grass: '74C236',
    ground: 'D3B357',
    ice: 'A3E7FD',
    normal: 'C8C4BC',
    poison: '934594',
    psychic: 'ED4882',
    rock: 'B9A156',
    steel: 'B5B5C3',
    water: '3295F6'
};

export default class Pokemon extends Component {

   state = {
       name: '',
       imageUrl: '',
       hp: "",
       attack: "",
       defense: "",
       speed: "",
       specialAttack: "",
       specialDefense: "",
       height: "",
       weight: "",
       primaryType: '',
       secondaryType: '',
       abilities: '',
       species: ''
   };

    /* istanbul ignore next */
   async componentDidMount() {
        const { pokemonIndex } = this.props.match.params;
        await axios.get(`https://online-pokedex-api.herokuapp.com/pokedex/api/v1/pokemon/${pokemonIndex}`)
            .then(pokemonRes => {
                const name = pokemonRes.data.name;
                const imageUrl = pokemonRes.data.sprite;
                const hp = pokemonRes.data.hp;
                const attack = pokemonRes.data.attack;
                const defense = pokemonRes.data.defense;
                const speed = pokemonRes.data.speed;
                const specialAttack = pokemonRes.data.special_attack;
                const specialDefense = pokemonRes.data.special_defense;
                const height = pokemonRes.data.height;
                const weight = pokemonRes.data.weight;
                const abilities = pokemonRes.data.abilities.map(ability => {
                    return ability
                        .split(',')
                        .map(s => s.charAt(0).toUpperCase() + s.substring(1))
                        .join(' ');
                }).join(', ');
                const primaryType = pokemonRes.data.primary_type;
                const secondaryType = pokemonRes.data.secondary_type;
                const species = pokemonRes.data.species;
                this.setState({
                    imageUrl: imageUrl,
                    pokemonIndex:pokemonIndex,
                    name: name,
                    hp: hp,
                    attack: attack,
                    defense: defense,
                    speed: speed,
                    specialAttack: specialAttack,
                    specialDefense: specialDefense,
                    height: height,
                    weight: weight,
                    primaryType: primaryType,
                    secondaryType: secondaryType,
                    abilities: abilities,
                    species: species
                });
            }).catch((error) => {
                console.log(error)
            })
    }

    render() {
        return (
            <div className="col">
                <div className="card">
                    <div className="card-header">
                        <div className="row">
                            <div className="col-5">
                                <h5>{this.state.pokemonIndex}</h5>
                            </div>
                            <div className="col-7">
                                <div className="float-right">
                                    <span key={this.state.primaryType}
                                        className="badge badge-primary badge-pill mr-1"
                                        style={{
                                            backgroundColor: `#${TYPE_COLORS[this.state.primaryType.toLowerCase()]}`,
                                            color: 'white'
                                        }}
                                    >
                                        {this.state.primaryType}
                                    </span>
                                </div>
                                <div className="float-right">
                                    <span key={this.state.secondaryType}
                                        className="badge badge-primary badge-pill mr-1"
                                        style={{
                                            backgroundColor: `#${TYPE_COLORS[this.state.secondaryType.toLowerCase()]}`,
                                            color: 'white'
                                        }}
                                    >
                                        {this.state.secondaryType}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row align-items-center">
                            <div className="col-md-3">
                                <img
                                    src={this.state.imageUrl}
                                    className="card-img-top rounded mx-auto mt-2"
                                    alt="pokemon" />
                            </div>
                            <div className="col-md-9">
                                <h4 className="mx-auto">
                                    {this.state.name
                                        .toLowerCase()
                                        .split('-')
                                        .map(s => s.charAt(0).toUpperCase() + s.substring(1))
                                        .join(' ')
                                    }
                                </h4>
                                <div className="row align-items-center">
                                    <div className="col-12 col-md-3">HP</div>
                                    <div className="col-12 col-md-9">
                                        <div className="progress">
                                            <div
                                                className="progress-bar"
                                                role="progressbar"
                                                style={{
                                                    width: `${this.state.hp}%`
                                                }}
                                                aria-valuenow="25"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                <small>{this.state.hp}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-12 col-md-3">Attack</div>
                                    <div className="col-12 col-md-9">
                                        <div className="progress">
                                            <div
                                                className="progress-bar"
                                                role="progressbar"
                                                style={{
                                                    width: `${this.state.attack}%`
                                                }}
                                                aria-valuenow="25"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                <small>{this.state.attack}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-12 col-md-3">Defense</div>
                                    <div className="col-12 col-md-9">
                                        <div className="progress">
                                            <div
                                                className="progress-bar"
                                                role="progressbar"
                                                style={{
                                                    width: `${this.state.defense}%`
                                                }}
                                                aria-valuenow="25"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                <small>{this.state.defense}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-12 col-md-3">Speed</div>
                                    <div className="col-12 col-md-9">
                                        <div className="progress">
                                            <div
                                                className="progress-bar"
                                                role="progressbar"
                                                style={{
                                                    width: `${this.state.speed}%`
                                                }}
                                                aria-valuenow="25"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                <small>{this.state.speed}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-12 col-md-3">Special Attack</div>
                                    <div className="col-12 col-md-9">
                                        <div className="progress">
                                            <div
                                                className="progress-bar"
                                                role="progressbar"
                                                style={{
                                                    width: `${this.state.specialAttack}%`
                                                }}
                                                aria-valuenow="25"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                <small>{this.state.specialAttack}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-12 col-md-3">Special Defense</div>
                                    <div className="col-12 col-md-9">
                                        <div className="progress">
                                            <div
                                                className="progress-bar"
                                                role="progressbar"
                                                style={{
                                                    width: `${this.state.specialDefense}%`
                                                }}
                                                aria-valuenow="25"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                            >
                                                <small>{this.state.specialDefense}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="card-body">
                        <h5 className="card-title text-center">Profile</h5>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h6 className="float-right">Height:</h6>
                                    </div>
                                    <div className="col-md-6">
                                        <h6 className="float-left">{this.state.height} ft.</h6>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <h6 className="float-right">Weight:</h6>
                                    </div>
                                    <div className="col-md-6">
                                        <h6 className="float-left">{this.state.weight} lbs.</h6>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <h6 className="float-right">Abilities:</h6>
                                    </div>
                                    <div className="col-md-6">
                                        <h6 className="float-left">{this.state.abilities}</h6>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <h6 className="float-right">Species:</h6>
                                    </div>
                                    <div className="col-md-6">
                                        <h6 className="float-left">{this.state.species}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
