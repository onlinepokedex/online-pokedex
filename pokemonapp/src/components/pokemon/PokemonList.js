import React, { Component } from 'react';
import PokemonCard from "./PokemonCard";
import axios from 'axios';


export default class PokemonList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            url: `https://online-pokedex-api.herokuapp.com/pokedex/api/v1/pokemon/`,
            pokemon: [],
            visiable: 12
        };
        this.loadMore = this.loadMore.bind(this);
    }
    /* istanbul ignore next */
    async componentDidMount() {
        await axios.get(this.state.url).then(res => {
            this.setState({ pokemon: res.data });
        }).catch(error => {
            console.log(error);
        })
    }

    /* istanbul ignore next */
    loadMore(){
        this.setState((old) => {
            return {visiable: old.visiable + 12}
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    {this.state.pokemon.slice(0, this.state.visiable).map(pokemon =>
                        <PokemonCard
                            key={pokemon.name}
                            name={pokemon.name}
                            url={pokemon.id}
                        />
                    )}
                </div>
                <hr/>
                <div className="col-md-12 text-center mb-4">
                    {this.state.visiable < this.state.pokemon.length &&
                    <button type="button" onClick={this.loadMore} className="btn btn-primary">Load More</button>
                    }
                </div>
            </div>
        )
    }
}
