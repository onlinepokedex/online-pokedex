import React, {useContext, useState} from 'react';


export const CustomAuthContext = React.createContext();
/* istanbul ignore next */
export const useCustomAuth = () => useContext(CustomAuthContext);
/* istanbul ignore next */
export const CustomAuthProvider = ({
  children,
  ...props
}) => {
  const existingUser = JSON.parse(localStorage.getItem('user'));
  const [user, setUser] = useState(existingUser);

  /* istanbul ignore next */
  const storeUser = (userData) => {
    localStorage.setItem('user', JSON.stringify(userData));
    setUser(userData);
  }
  /* istanbul ignore next */
  const logOut = () => {
    localStorage.removeItem('user');
    setUser(undefined);
  }

  /* istanbul ignore next */
  return (
    <CustomAuthContext.Provider value={{
      user,
      setUser : storeUser,
      logOut
    }}>
    {children}
    </CustomAuthContext.Provider>
  )
}
